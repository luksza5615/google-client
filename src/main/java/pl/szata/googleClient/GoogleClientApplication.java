package pl.szata.googleClient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.szata.googleClient.configuration.DownloadConfigurationProperties;

@SpringBootApplication
public class GoogleClientApplication implements CommandLineRunner {

    @Autowired
    DownloadConfigurationProperties downloadConfigurationProperties;

    public static void main(String[] args) {
        SpringApplication.run(GoogleClientApplication.class, args);
    }

    @Override
    public void run(String... args) {
        System.out.println("Output directory: " + downloadConfigurationProperties.getOutputDirectory());
        System.out.println("Working directory: " + downloadConfigurationProperties.getWorkingDirectory());
    }
}
