package pl.szata.googleClient.service;

import com.github.kiulian.downloader.YoutubeDownloader;
import com.github.kiulian.downloader.YoutubeException;
import com.github.kiulian.downloader.model.VideoDetails;
import com.github.kiulian.downloader.model.YoutubeVideo;
import com.github.kiulian.downloader.model.formats.Format;
import org.springframework.stereotype.Service;
import pl.szata.googleClient.configuration.ConvertConfigurationProperties;
import pl.szata.googleClient.configuration.DownloadConfigurationProperties;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Scanner;

@Service
public class DownloaderService {

    private final DownloadConfigurationProperties downloadConfigurationProperties;
    private final ConvertConfigurationProperties convertConfigurationProperties;
    private YoutubeDownloader downloader = new MyDownloader();

    public DownloaderService(DownloadConfigurationProperties downloadConfigurationProperties, ConvertConfigurationProperties convertConfigurationProperties) {
        this.downloadConfigurationProperties = downloadConfigurationProperties;
        this.convertConfigurationProperties = convertConfigurationProperties;
    }

    private void printVideoDetails(YoutubeVideo youtubeVideo) {
        VideoDetails details = youtubeVideo.details();

        System.out.println("title " + details.title());
        System.out.println("view count: " + details.viewCount());
        System.out.println("author:" + details.author());
        System.out.println("short description: " + details.description());
        System.out.println("average rating :" + details.averageRating());
        System.out.println("isLive: " + details.isLive());
        System.out.print("keywords: ");
        for (String keyword : details.keywords()) {
            System.out.print(keyword + " ");
        }
        System.out.println();
    }

    private void printVideoFormatsDetails(YoutubeVideo youtubeVideo) {
        System.out.println("formats: ");
        for (Format format : youtubeVideo.formats()) {
//            System.out.print(format.url());
//            System.out.print("Format URL: " + format.url());
            System.out.print("Format mimeType: " + format.mimeType() + '\t');
            System.out.print("Format bitrate: " + format.bitrate() + '\t');
            System.out.print("Format content length: " + format.contentLength() + '\t');
            System.out.print("Format lastModified: " + format.lastModified() + '\t');
            System.out.print("Format duration: " + format.duration() + '\t');
            System.out.print("Format extension: " + format.extension().value() + '\t');
            System.out.print("Format itag: " + format.itag().toString() + '\t');
            System.out.print("Format itag name: " + format.itag().name() + '\t');
            System.out.print("Format itag audioquality: " + format.itag().audioQuality().name() + '\t');
            System.out.print("Format itag videoquality: " + format.itag().videoQuality().name() + '\t');
            System.out.println();
        }
    }

    private void convertVideoToMp3(YoutubeVideo youtubeVideo, File file) throws IOException, InterruptedException {
        String name = file.getName();
        String baseNameWithoutExtension = name.substring(0, name.length() - 4);
        String author = youtubeVideo.details().author();
        String adjustedName = baseNameWithoutExtension + "_A_" + author + ".mp3";

        //works only with /C and one " at the end
        String command = "/C ffmpeg -i " + "\"" + name + "\"" + " -vn -s 00:00:10 -acodec libmp3lame " + "\"" + adjustedName;

        ProcessBuilder pb = new ProcessBuilder("cmd.exe", command);
        pb.directory(new File(downloadConfigurationProperties.getWorkingDirectory()));

        //logging
        File logFile = new File(convertConfigurationProperties.getLogDirectory(), "Log.txt");
        pb.redirectOutput(logFile);
        pb.redirectErrorStream(true); //error merged to common log

        Process process = pb.start();
        IOThreadHandler outputHandler = new IOThreadHandler(
                process.getInputStream());
        outputHandler.start();
        process.waitFor();
        System.out.println(outputHandler.getOutput());
    }

    public void downloadYoutubeVideoAndConvert(String videoId) throws IOException, YoutubeException, InterruptedException {
//        String videoURL1 = "https://www.youtube.com/watch?v=pcPi4jPAR2c"; // for url https://www.youtube.com/watch?v=abc12345 (just ending)
//        YoutubeVideo video = downloader.getVideo(videoURL1); //alternative by whole url

        YoutubeVideo video = downloader.getVideo(videoId);

        printVideoDetails(video);
        printVideoFormatsDetails(video);

        File outputDir = new File(downloadConfigurationProperties.getOutputDirectory());
        List<Format> formats = video.formats();
        File file = video.download(formats.get(0), outputDir);
        File downloadedFile = new File(file.getName());
        convertVideoToMp3(video, downloadedFile);
    }

    private static class IOThreadHandler extends Thread {
        private InputStream inputStream;
        private StringBuilder output = new StringBuilder();

        IOThreadHandler(InputStream inputStream) {
            this.inputStream = inputStream;
        }

        public void run() {
            Scanner br = null;
            try {
                br = new Scanner(new InputStreamReader(inputStream));
                String line = null;
                while (br.hasNextLine()) {
                    line = br.nextLine();
                    output.append(line
                            + System.getProperty("line.separator"));
                }
            } finally {
                br.close();
            }
        }

        public StringBuilder getOutput() {
            return output;
        }
    }
}
