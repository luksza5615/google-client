package pl.szata.googleClient.service;

import com.alibaba.fastjson.JSONObject;
import com.github.kiulian.downloader.YoutubeDownloader;
import com.github.kiulian.downloader.YoutubeException;
import com.github.kiulian.downloader.cipher.CipherFunction;
import com.github.kiulian.downloader.model.VideoDetails;
import com.github.kiulian.downloader.model.YoutubeVideo;
import com.github.kiulian.downloader.model.formats.Format;
import com.github.kiulian.downloader.parser.DefaultParser;
import com.github.kiulian.downloader.parser.Parser;

import java.io.IOException;
import java.util.List;

public class MyDownloader extends YoutubeDownloader {

    private Parser parser;

    public MyDownloader() {
        this.parser = new DefaultParser();
    }

    public MyDownloader(Parser parser) {
        this.parser = parser;
    }

    public void setParserRequestProperty(String key, String value) {
        parser.getExtractor().setRequestProperty(key, value);
    }

    public void setParserRetryOnFailure(int retryOnFailure) {
        parser.getExtractor().setRetryOnFailure(retryOnFailure);
    }

    public void addCipherFunctionPattern(int priority, String regex) {
        parser.getCipherFactory().addInitialFunctionPattern(priority, regex);
    }

    public void addCipherFunctionEquivalent(String regex, CipherFunction function) {
        parser.getCipherFactory().addFunctionEquivalent(regex, function);
    }

    @Override
    public YoutubeVideo getVideo(String videoId) throws YoutubeException, IOException {
        String htmlUrl = "https://www.youtube.com/watch?v=" + videoId; //other way

        JSONObject ytPlayerConfig = parser.getPlayerConfig(htmlUrl);

        VideoDetails videoDetails = parser.getVideoDetails(ytPlayerConfig);

        List<Format> formats = parser.parseFormats(ytPlayerConfig);
        return new YoutubeVideo(videoDetails, formats);
    }
}
