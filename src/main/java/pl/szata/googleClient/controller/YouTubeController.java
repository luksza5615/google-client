package pl.szata.googleClient.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import pl.szata.googleClient.model.videoCategoryModel.VideoCategory;
import pl.szata.googleClient.model.youTubeChannelModel.YouTubeChannel;

@RestController
public class YouTubeController {


    @GetMapping("/channels")
    public YouTubeChannel getAllYouTubeChannels() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        ResponseEntity exchange = restTemplate.exchange("https://www.googleapis.com/youtube/v3/channels?part=snippet,statistics,brandingSettings,contentOwnerDetails,topicDetails&id=UCPWrV7_5uU_U_vfrLhNa0lg&key=AIzaSyC8MucNHSLMz27R3qZVNEH_lubEcnu8lwQ",
                HttpMethod.GET,
                httpEntity,
                YouTubeChannel.class);

        return (YouTubeChannel)exchange.getBody();
    }

    @GetMapping("/category")
    public VideoCategory getChannelInfo() {

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        ResponseEntity exchange = restTemplate.exchange("https://www.googleapis.com/youtube/v3/videoCategories?part=snippet&regionCode=US&key=AIzaSyC8MucNHSLMz27R3qZVNEH_lubEcnu8lwQ",
                HttpMethod.GET,
                httpEntity,
                VideoCategory.class);

        return (VideoCategory)exchange.getBody();

    }

}
