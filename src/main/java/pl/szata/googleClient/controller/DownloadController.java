package pl.szata.googleClient.controller;

import com.github.kiulian.downloader.YoutubeException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.szata.googleClient.service.DownloaderService;

import java.io.IOException;

@Controller
public class DownloadController {

    private DownloaderService downloaderService;

    public DownloadController(DownloaderService downloaderService) {
        this.downloaderService = downloaderService;
    }

    @GetMapping("/download/{videoId}")
    public void downloadVideoAndConvert(@PathVariable String videoId) throws InterruptedException, YoutubeException, IOException {
        downloaderService.downloadYoutubeVideoAndConvert(videoId);
    }

}
