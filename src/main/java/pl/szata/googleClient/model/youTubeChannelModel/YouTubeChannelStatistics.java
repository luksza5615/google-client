package pl.szata.googleClient.model.youTubeChannelModel;

public class YouTubeChannelStatistics {
    private String viewCount;
    private String commentCount;
    private String subscribersCount;
    private Boolean hiddenSubscriberCount;
    private String videoCount;

    public String getViewCount() {
        return viewCount;
    }

    public void setViewCount(String viewCount) {
        this.viewCount = viewCount;
    }

    public String getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(String commentCount) {
        this.commentCount = commentCount;
    }

    public String getSubscribersCount() {
        return subscribersCount;
    }

    public void setSubscribersCount(String subscribersCount) {
        this.subscribersCount = subscribersCount;
    }

    public String getVideoCount() {
        return videoCount;
    }

    public void setVideoCount(String videoCount) {
        this.videoCount = videoCount;
    }

    public Boolean getHiddenSubscriberCount() {
        return hiddenSubscriberCount;
    }

    public void setHiddenSubscriberCount(Boolean hiddenSubscriberCount) {
        this.hiddenSubscriberCount = hiddenSubscriberCount;
    }
}
