package pl.szata.googleClient.model.youTubeChannelModel;

public class YouTubeChannelSnippet {
    private String title;
    private String description;
    private String publishedAt;
    private String customUrl;
    private String defaultLanguage;
    private YouTubeChannelLocalized localized;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    private String country;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCustomUrl() {
        return customUrl;
    }

    public void setCustomUrl(String customUrl) {
        this.customUrl = customUrl;
    }

    public String getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(String defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public YouTubeChannelLocalized getLocalized() {
        return localized;
    }

    public void setLocalized(YouTubeChannelLocalized localized) {
        this.localized = localized;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }
}
