package pl.szata.googleClient.model.youTubeChannelModel;

public class YouTubeCHannelBrandingSettingsChannel {
    private String title;
    private String description;
    private String keywords;
    private String defaultTab;
    private String trackingAnalyticsAccountId;
    private Boolean moderateComments;
    private Boolean showRelatedChannels;
    private Boolean showBrowseView;
    private String featuredChannelsTitle;
    private String defaultLanguage;
    private String country;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getDefaultTab() {
        return defaultTab;
    }

    public void setDefaultTab(String defaultTab) {
        this.defaultTab = defaultTab;
    }

    public String getTrackingAnalyticsAccountId() {
        return trackingAnalyticsAccountId;
    }

    public void setTrackingAnalyticsAccountId(String trackingAnalyticsAccountId) {
        this.trackingAnalyticsAccountId = trackingAnalyticsAccountId;
    }

    public Boolean getModerateComments() {
        return moderateComments;
    }

    public void setModerateComments(Boolean moderateComments) {
        this.moderateComments = moderateComments;
    }

    public Boolean getShowRelatedChannels() {
        return showRelatedChannels;
    }

    public void setShowRelatedChannels(Boolean showRelatedChannels) {
        this.showRelatedChannels = showRelatedChannels;
    }

    public Boolean getShowBrowseView() {
        return showBrowseView;
    }

    public void setShowBrowseView(Boolean showBrowseView) {
        this.showBrowseView = showBrowseView;
    }

    public String getFeaturedChannelsTitle() {
        return featuredChannelsTitle;
    }

    public void setFeaturedChannelsTitle(String featuredChannelsTitle) {
        this.featuredChannelsTitle = featuredChannelsTitle;
    }

    public String getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(String defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
