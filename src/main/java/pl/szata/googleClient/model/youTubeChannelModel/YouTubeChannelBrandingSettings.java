package pl.szata.googleClient.model.youTubeChannelModel;

public class YouTubeChannelBrandingSettings {
    private YouTubeCHannelBrandingSettingsChannel channel;

    public YouTubeCHannelBrandingSettingsChannel getChannel() {
        return channel;
    }

    public void setChannel(YouTubeCHannelBrandingSettingsChannel channel) {
        this.channel = channel;
    }
}
