package pl.szata.googleClient.model.youTubeChannelModel;

import java.util.List;

public class YouTubeChannel {
    private String kind;
    private String nextPageToken;
    private String prevPageToken;
    private List<YouTubeChannelItem> items;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getNextPageToken() {
        return nextPageToken;
    }

    public void setNextPageToken(String nextPageToken) {
        this.nextPageToken = nextPageToken;
    }

    public String getPrevPageToken() {
        return prevPageToken;
    }

    public void setPrevPageToken(String prevPageToken) {
        this.prevPageToken = prevPageToken;
    }

    public List<YouTubeChannelItem> getItems() {
        return items;
    }

    public void setItems(List<YouTubeChannelItem> items) {
        this.items = items;
    }
}
