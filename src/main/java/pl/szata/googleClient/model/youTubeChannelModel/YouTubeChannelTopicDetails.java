package pl.szata.googleClient.model.youTubeChannelModel;

import java.util.List;

public class YouTubeChannelTopicDetails {
    private List<String> topicIds;
    private List<String> topicCategories;

    public List<String> getTopicIds() {
        return topicIds;
    }

    public void setTopicIds(List<String> topicIds) {
        this.topicIds = topicIds;
    }

    public List<String> getTopicCategories() {
        return topicCategories;
    }

    public void setTopicCategories(List<String> topicCategories) {
        this.topicCategories = topicCategories;
    }
}
