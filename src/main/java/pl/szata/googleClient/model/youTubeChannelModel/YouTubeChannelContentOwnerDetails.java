package pl.szata.googleClient.model.youTubeChannelModel;

public class YouTubeChannelContentOwnerDetails {
    private String contentOwner;

    public String getContentOwner() {
        return contentOwner;
    }

    public void setContentOwner(String contentOwner) {
        this.contentOwner = contentOwner;
    }
}
