package pl.szata.googleClient.model.youTubeChannelModel;

public class YouTubeChannelItem {
    private String id;
    private YouTubeChannelSnippet snippet;
    private YouTubeChannelStatistics statistics;
    private YouTubeChannelBrandingSettings brandingSettings;
    private YouTubeChannelContentOwnerDetails contentOwnerDetails;
    private YouTubeChannelTopicDetails topicDetails;

    public YouTubeChannelStatistics getStatistics() {
        return statistics;
    }

    public void setStatistics(YouTubeChannelStatistics statistics) {
        this.statistics = statistics;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public YouTubeChannelSnippet getSnippet() {
        return snippet;
    }

    public void setSnippet(YouTubeChannelSnippet snippet) {
        this.snippet = snippet;
    }

    public YouTubeChannelBrandingSettings getBrandingSettings() {
        return brandingSettings;
    }

    public void setBrandingSettings(YouTubeChannelBrandingSettings brandingSettings) {
        this.brandingSettings = brandingSettings;
    }

    public YouTubeChannelContentOwnerDetails getContentOwnerDetails() {
        return contentOwnerDetails;
    }

    public void setContentOwnerDetails(YouTubeChannelContentOwnerDetails contentOwnerDetails) {
        this.contentOwnerDetails = contentOwnerDetails;
    }

    public YouTubeChannelTopicDetails getTopicDetails() {
        return topicDetails;
    }

    public void setTopicDetails(YouTubeChannelTopicDetails topicDetails) {
        this.topicDetails = topicDetails;
    }
}
