package pl.szata.googleClient.model.videoCategoryModel;

public class VideoCategoryItem {

    private String kind;
    private String id;
    private VideoCategorySnippet snippet;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public VideoCategorySnippet getSnippet() {
        return snippet;
    }

    public void setSnippet(VideoCategorySnippet snippet) {
        this.snippet = snippet;
    }
}
