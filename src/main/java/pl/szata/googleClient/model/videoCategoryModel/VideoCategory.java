package pl.szata.googleClient.model.videoCategoryModel;


import java.util.List;

public class VideoCategory {

    private String kind;
    private String nextPageToken;
    private String prevPageToken;
    private PageInfo pageInfo;
    private List<VideoCategoryItem> items;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getNextPageToken() {
        return nextPageToken;
    }

    public void setNextPageToken(String nextPageToken) {
        this.nextPageToken = nextPageToken;
    }

    public String getPrevPageToken() {
        return prevPageToken;
    }

    public void setPrevPageToken(String prevPageToken) {
        this.prevPageToken = prevPageToken;
    }

    public PageInfo getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfo pageInfo) {
        this.pageInfo = pageInfo;
    }

    public List<VideoCategoryItem> getItems() {
        return items;
    }

    public void setVideoCategoryItems(List<VideoCategoryItem> items) {
        this.items = items;
    }
}
